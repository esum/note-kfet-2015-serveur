#!/usr/bin/python
#-*- coding: utf-8 -*-

from __future__ import unicode_literals, print_function

import sys

import psycopg2, psycopg2.extras
from jinja2 import Environment, PackageLoader

if '/home/note/note-kfet-2015-serveur/' not in sys.path:
    sys.path.append('/home/note/note-kfet-2015-serveur/')
import mail

sys.path.append('../serveur')
import BaseFonctions

if __name__ == '__main__':
    # On initialise le template
    env = Environment(loader=PackageLoader('mail', 'templates'))
    template = env.get_template('template_wei_1a')

    # On prépare la connexion à la base de données
    con, cur = BaseFonctions.getcursor()

    # On récupère les données pertinentes
    cur.execute("SELECT * FROM wei_1a WHERE NOT triggered;")
    liste_1a = cur.fetchall()

    cur.execute("""SELECT prix_wei_normalien,
                        prix_wei_non_normalien, 
                        wei_name, 
                        wei_contact, 
                        wei_begin, 
                        wei_end 
                 FROM configurations;""")
    wei_config = cur.fetchall()[0]
    wei_config["prix_wei_normalien"]/=100
    wei_config["prix_wei_non_normalien"]/=100

    
    # Paramètres commun à tous les mails
    subject = "Inscription au WEI"
    emetteur = "notekfet2015@crans.org"
    reply_to = [wei_config["wei_contact"].decode('utf-8')]

    for entry in liste_1a:
        entry["prenom"] = entry["prenom"]
        entry["nom"] = entry["nom"]

        to = [entry["mail"],]
        body = template.render(**{ 'user' : entry, 'wei' : wei_config })

        mail.queue_mail(emetteur, to, subject, body, cc=[], replyto=reply_to)

        cur.execute("UPDATE wei_1a SET triggered = TRUE WHERE idwei=%s;", (entry["idwei"],))

        cur.execute("COMMIT;")
