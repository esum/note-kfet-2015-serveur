CREATE TABLE queue (
id SERIAL CONSTRAINT id_queue_est_un_id PRIMARY KEY,
queue_date timestamp DEFAULT now(),
emetteur varchar DEFAULT 'notekfet2015@crans.org',
destinataires varchar DEFAULT 'nobody@crans.org',
subject varchar DEFAULT '[Note Kfet] <Empty Subject>',
body varchar DEFAULT '',
cc varchar DEFAULT ''
);

CREATE TABLE sent (
id SERIAL CONSTRAINT id_sent_est_un_id PRIMARY KEY,
sent_date timestamp DEFAULT now(),
queue_date timestamp,
emetteur varchar DEFAULT 'notekfet2015@crans.org',
destinataires varchar DEFAULT 'nobody@crans.org',
subject varchar DEFAULT '[Note Kfet] <Empty Subject>',
body varchar DEFAULT '',
cc varchar DEFAULT ''
);
