#!/usr/bin/python
#-*- coding: utf-8 -*-

###
# Envoie un mail au vieux pour leur prévenir qu'on va se débarrasser de leurs chèques.
# (très inspiré de wei_vieux.py)
# @PAC
###


from __future__ import unicode_literals

import sys
from datetime import datetime, timedelta

from jinja2 import Environment, PackageLoader
import psycopg2, psycopg2.extras

if '/home/note/note-kfet-2015-serveur/mail/' not in sys.path:
    sys.path.append('/home/note/note-kfet-2015-serveur/mail/')
from mail import queue_mail

sys.path.append('../serveur')
import BaseFonctions

# Initialise la connexion à la base de données
con, cur = BaseFonctions.getcursor()

# Récupère les informations du wei
cur.execute("SELECT wei_name, wei_contact FROM configurations;")
wei_config = cur.fetchall()[0]

emetteur = 'notekfet2015@crans.org'
objet = '[WEI] Retour des cautions Wei'
reply_to = [wei_config["wei_contact"].decode('utf-8')]

if __name__ == '__main__':
    #  Chargement du template de mail

    env = Environment(loader=PackageLoader('mail', 'templates'))

    template = env.get_template('template_retour_caution_wei')

    # Récupération des inscriptions dans la base
    cur.execute("SELECT * FROM wei_vieux WHERE caution = 't';")

    liste_inscriptions = cur.fetchall()

    # On traite les inscriptions
    for inscription in liste_inscriptions:
        paye = inscription['paye']
        caution = inscription['caution']
        if paye and caution:
            contexte = {
                'user' : user,
                'caution' : caution,
                'wei' : wei_config,
            }
            body = template.render(**contexte)
            queue_mail(emetteur, [inscription["mail"],], objet, body, cc=[], replyto=reply_to)
