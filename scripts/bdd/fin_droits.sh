#!/bin/bash

# Pour remettre les droits à zéro en fin de mandat

# On remet les droits à basic, sauf pour l'ordi kfet et l'ordi tna. Il se trouve que les idbde<=0 ont aussi 'basic','',false donc ne sont pas à traiter à part
psql note -c "UPDATE comptes SET droits='basic', surdroits='', supreme=false WHERE idbde NOT IN (3508,3932);"
# Ordi kfet
psql note -c "UPDATE comptes SET droits='login,preinscriptions,liste_droits,note,forced', surdroits='', supreme=false WHERE idbde = 3508;"
# Ordi tna
psql note -c "UPDATE comptes SET droits='login,liste_droits,note,forced', surdroits='', supreme=false WHERE idbde = 3932;"
