
# -*- coding: utf-8 -*-

"""Définitions des différentes exceptions propres à la Note.

Elles héritent toutes de :py:class:`NoteKfetError`.

"""

class NoteKfetError(Exception):
    """Erreur de base de la NoteKfet"""
    def __init__(self, msg=u""):
        Exception.__init__(self, msg)

class JsonError(NoteKfetError):
    """Erreur levée en cas d'échec de Json"""
    def __init__(self, msg):
        NoteKfetError.__init__(self, u"JSON-foirage " + msg)

class DeadClient(NoteKfetError):
    """Erreur levée quand on ne lit plus rien depuis le client."""
    pass

class RequestTooLong(NoteKfetError):
    """Erreur levée quand on a trop lu sur la socket et que le paquet n'est toujours pas parsable."""
    pass

class BadDebugTextType(NoteKfetError):
    """Erreur levée en cas de tentative d'affichage en débug d'un texte qui n'est pas une chaîne unicode."""
    def __init__(self):
        NoteKfetError.__init__(self, u'Must debug with unicode')

class TuTeFousDeMaGueule(NoteKfetError):
    """Erreur qui ne devrait jamais être levée."""
    def __init__(self, msg):
        NoteKfetError.__init__(self)
        self.msg = msg
    def __str__(self):
        return NoteKfetError.__str__(self) + self.msg

class Error404(NoteKfetError):
    """Erreur levée quand un bouton/un compte/… n'existe pas."""
    def __init__(self, msg):
        NoteKfetError.__init__(self, msg)

class AdhesionExpired(NoteKfetError):
    """Erreur levée pendant une transaction quand le compte n'est pas à jour d'adhésion."""
    def __init__(self, idbde):
        NoteKfetError.__init__(self, "Le compte %s n'est plus adhérent" % idbde)
        self.idbde = idbde

class AccountBlocked(NoteKfetError):
    """Erreur levée pendant une transaction quand le compte est bloqué."""
    def __init__(self, idbde):
        NoteKfetError.__init__(self, "Le compte %s est bloqué" % idbde)
        self.idbde = idbde

class PgParseError(NoteKfetError):
    """Erreur levée quand on tombe sur un objet qu'on ne sait pas pré-convertir pour qu'il soit JSON-izable."""
    def __init__(self, msg):
        NoteKfetError.__init__(self, msg)

class NoSuchServer(NoteKfetError):
    """Erreur levée quand on cherche à parler à un :py:class:`Serveur.Server` qui n'existe pas/plus."""
    def __init__(self, numero):
        NoteKfetError.__init__(self, "Ce Server n'existe pas ou n'est pas actif %s" % (numero,))

class IntegrityError(NoteKfetError):
    """Erreur levée quand on détecte une incohérence dans la base."""
    def __init__(self, msg):
        NoteKfetError.__init__(self, msg)

class EmptyResult(NoteKfetError):
    """Erreur levée lorsqu'on demande un pretty print de rien."""
    def __init__(self, msg):
        NoteKfetError.__init__(self, msg)

class WaitNextCommand(NoteKfetError):
    """Erreur levée lorsqu'une fonction veut dire au :py:class:`Serveur.Server` qu'il
       doit abandonner tout le flux d'exécution et simplement attendre la commande
       suivante du client."""
    pass
